package name

import "testing"

func TestSum(t *testing.T) {

	// usage
	{
		Sum()
		Sum(1)
		Sum(1, 2)
		Sum(1, 2, 3)
		Sum([]int{0}...)
	}

	type TestCase struct {
		arr []int
		sum int
	}

	tests := []TestCase{
		{[]int{}, 0},
		{[]int{0, 0, 0}, 0},
		{[]int{123}, 123},
		{[]int{1, 1, 1, 1}, 4},
		{[]int{1, 2, 3}, 6},
	}

	for i, test := range tests {
		if Sum(test.arr...) != test.sum {
			t.Errorf("Fail: %d", i)
		}
	}
}
